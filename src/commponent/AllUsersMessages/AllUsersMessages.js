import React, {Fragment} from 'react';
import Divider from '@material-ui/core/Divider';
import Message from '../Message/Message';
import {array, func} from 'prop-types';

const AllUsersMessages = ({messages, editMessage, deleteMessage, likeMessage}) => {
    const createAllUsersMessages = () => {
        return messages.map((post, index) => {
            if (messages[index + 1] && (new Date(messages[index + 1].createdAt)).getDate() - (new Date(messages[index].createdAt)).getDate()) {

                return (
                    <Fragment key={post.id}>
                        <Message post={post} editMessage={editMessage} deleteMessage={deleteMessage}
                                 likeMessage={likeMessage}/>
                        <Divider style={{marginTop: 20}}/>
                        <p style={{textAlign: 'center'}}>{(new Date(messages[index + 1].createdAt))
                            .toLocaleString('en', {month: 'long', day: 'numeric'
                        })}</p>
                    </Fragment>
                )
            } else {
                return <Message key={post.id} post={post} editMessage={editMessage} deleteMessage={deleteMessage}
                                likeMessage={likeMessage}/>;
            }
        })
    }

    const allUsersMessages = createAllUsersMessages();

    return (
        <>
            {allUsersMessages}
        </>
    )
}

AllUsersMessages.propTypes = {
    messages: array.isRequired,
    editMessage: func.isRequired,
    deleteMessage: func.isRequired,
    likeMessage: func.isRequired
}

export default AllUsersMessages;

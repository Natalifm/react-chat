import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import {messageService} from "../MessageService";
import Header from "../Header/Header";
import AllUsersMessages from "../AllUsersMessages/AllUsersMessages";
import TextInput from "../TextInput/TextInput"
import './App.css';
import Logo from "../Logo/Logo";
import Footer from "../Footer/Footer";

class App extends Component {
    state = {
        isLoading: false,
        messages: []
    }

    async componentDidMount() {
        const response = await messageService.fetchMessages('https://edikdolynskyi.github.io/react_sources/messages.json');
        const messages = await response.json();
        this.setState({
            messages
        });
    }

    createNewMessage = (message) => {
        this.setState(({messages}) => ({
            messages: [...messages, message],
        }))
    }

    editMessage = (id, updatedMessage) => {
        const {messages} = this.state;
        this.setState({
            messages: messages.map(post => post.id === id
                ? {
                    ...post,
                    message: updatedMessage,
                }
                : post
            )
        })
    }

    deleteMessage = (id) => {
        const {messages} = this.state;
        this.setState({
            messages: messages.filter(message => message.id !== id),
        })
    }

    likeMessage = (id) => {
        const {messages} = this.state;
        this.setState({
            messages: messages.map(message => message.id === id
                ? {
                    ...message,
                    isLiked: true,
                }
                : message
            )
        })
    }

    render() {
        const {messages} = this.state;
        const participants = (new Set(messages.map(({user}) => user))).size;
        const lastMessageTime = messages.length && messages[messages.length - 1].createdAt;

        return (
            <>
                {messages.length
                    ? <>
                        <Logo/>
                        <Header
                            usersmessages={messages.length}
                            participants={participants}
                            lastMessageTime={lastMessageTime}
                        />
                        <CssBaseline/>
                        <Container maxWidth="md">
                            <AllUsersMessages
                                messages={messages}
                                deleteMessage={this.deleteMessage}
                                editMessage={this.editMessage}
                                likeMessage={this.likeMessage}
                            />
                            <TextInput
                                createNewMessage={this.createNewMessage}
                            />
                            <Footer/>
                        </Container>
                    </>
                    : <CircularProgress
                        className='loading'
                        color='secondary'
                        size={80}
                        thickness={6}
                    />
                }
            </>
        )
    }
}

export default App;

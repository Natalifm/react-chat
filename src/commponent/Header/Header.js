import React from 'react';
import {number, string} from 'prop-types';
import './Header.css';

const Header = ({participants, usersmessages, lastMessageTime}) => {
    return (
        <header>
            <div className='header-option'>
                <div>
                    My chat
                </div>
                <div>
                    {participants} participants
                </div>
                <div>
                    {usersmessages} messages
                </div>
            </div>
            <div className='last_time_message'>
                Last message at {lastMessageTime}
            </div>
        </header>
    )
}

Header.propTypes = {
    participants: number.isRequired,
    usersmessages: number.isRequired,
    lastMessageTime: string.isRequired,
}

export default Header;

import React from "react";
import './Logo.css'

const Logo = () => {
    return(
        <div className="chat-logo">
            <img src= "https://cdn.worldvectorlogo.com/logos/hipchat.svg" alt = "chatLogo"/>
            <div>
                <h1>MyChat</h1>
            </div>
        </div>
    )
}

export default Logo;
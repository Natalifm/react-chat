import React, {useState, createRef} from 'react';
import Card from '@material-ui/core/Card';
import Avatar from '@material-ui/core/Avatar';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import {Edit, Delete, FavoriteBorder} from '@material-ui/icons';
import {func, bool, string} from 'prop-types';
import './Message.css';

const Message = ({post, editMessage, deleteMessage, likeMessage}) => {
    const [isEditing, setIsEditing] = useState(false);
    const {id, avatar, createdAt, text, user, isLiked} = post;

    let inputRef = createRef();

    const favoriteBorderColor = isLiked ? '#f00' : '#000';
    const textAreaBorderColor = isEditing ? '#09b39a' : 'transparent';
    const myMessage = user === 'me' ? 'auto' : '0';

    const handleEditMessage = () => {
        setIsEditing(!isEditing);

        if (isEditing) {
            editMessage(id, inputRef.current.value);
        }
    }

    const createEditIcon = () => user === 'me'
        ? <Edit
            className='edit'
            onClick={handleEditMessage}/>
        : null;
    const createDeleteIcon = () => user === 'me'
        ? <Delete
            className='delete'
            onClick={() => deleteMessage(id)}/>
        : null;
    const favoriteBorderIcon = () => user !== 'me'
        ? <FavoriteBorder
            className='like'
            style={{color: favoriteBorderColor}}
            onClick={() => likeMessage(id)}/>
        : null;

    return (
        <Card
            className='card'
            style={{marginLeft: myMessage}}>
            {avatar && <Avatar src={avatar} alt="user" style={{margin: 10}}/>}
            <div style={{width: '80%', maxWidth: '100%'}}>
                <p>{createdAt}</p>
                <TextareaAutosize
                    className='textarea'
                    aria-label="Textarea"
                    rows={3}
                    defaultValue={text}
                    ref={inputRef}
                    disabled={!isEditing}
                    style={{border: `1px solid ${textAreaBorderColor}`}}
                />
            </div>
            {createEditIcon()}
            {createDeleteIcon()}
            {favoriteBorderIcon()}
        </Card>
    )
}

Message.propTypes = {
    post: {
        id: string.isRequired,
        avatar: string,
        createdAt: string.isRequired,
        text: string.isRequired,
        user: string.isRequired,
        editedAt: string,
        isLiked: bool
    },
    editMessage: func.isRequired,
    deleteMessage: func.isRequired,
    likeMessage: func.isRequired
}

export default Message;
